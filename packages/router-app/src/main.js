import '@app1/assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'  

import mainApp1 from '@app1/main.js' 
import mainApp2 from '@app2/main.js' 

const arr_mains = [
    mainApp1,
    mainApp2,
];

const mains = arr_mains.flat(1);

const app = createApp(App)

app.use(router)  

mains.forEach(main => {
    app.use(main) 
});

app.mount('#app')
