import MainLayout from '@app1/layouts/DefaultLayout.vue';

import HomeView from '../views/HomeView.vue'

export default [
  {
    path: '/home-2',
    name: 'home-2',
    component: HomeView,
    meta: {
      layout: MainLayout, 
    },
  },
  {
    path: '/about-2',
    name: 'about-2',
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/AboutView.vue'),
    meta: {
      layout: MainLayout, 
    },
  }
]
