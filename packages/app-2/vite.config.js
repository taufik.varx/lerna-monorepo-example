const path = require('path')

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

const app1 = path.resolve(__dirname, '../app-1/src')
const app2 = path.resolve(__dirname, '../app-2/src') 

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@app1': app1,
      '@app2': app2, 
    }
  }
})
