import MainLayout from '@app1/layouts/DefaultLayout.vue';

import HomeView from '@app1/views/HomeView.vue' 

export default [{
  path: '/home',
  name: 'home',
  component: HomeView,
  meta: {
    layout: MainLayout, 
  },
},
{
  path: '/about',
  name: 'about', 
  component: () => import('@app1/views/AboutView.vue'),
  meta: {
    layout: MainLayout, 
  },
}]